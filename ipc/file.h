#ifndef _IPC_FILE_H_
#define _IPC_FILE_H_

#include "ipc/ipc.h"

const ipc_functions_t *ipc_file_functions();

#endif // _IPC_FILE_H_
