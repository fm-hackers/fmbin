#ifndef _IPC_SOCKET_H_
#define _IPC_SOCKET_H_

#include "ipc/ipc.h"

const ipc_functions_t *ipc_socket_functions();

#endif // _IPC_SOCKET_H_
