#ifndef _IPC_IPC_H_
#define _IPC_IPC_H_

typedef struct server_config_t server_config_t;

/*
 * Interface that must be implemented by the IPC backend.
 */
typedef struct {
	int (*init)(const server_config_t *config, void **ctx);
	ssize_t (*read_request)(void *ctx, void *buffer, size_t count);
	ssize_t (*send_response)(void *ctx, const void *buffer, size_t count);
	ssize_t (*send_interruption)(void *ctx, const void *buffer, size_t count);
	int (*close)(void *ctx);
} ipc_functions_t;

#endif // _IPC_IPC_H_
